#include "SDK.h"
#include "Client.h"
#include "CAimbot.h"

//============================================================================================
bool __fastcall Hooked_CreateMove(PVOID ClientMode, int edx, float input_sample_frametime, CUserCmd* pCommand)
{
	VMTManager& hook = VMTManager::GetHook(gInts.ClientMode); //Get a pointer to the instance of your VMTManager with the function GetHook.
	bool bReturn = hook.GetMethod<bool(__thiscall*)(PVOID, float, CUserCmd*)>(gOffsets.iCreateMoveOffset)(ClientMode, input_sample_frametime, pCommand); //Call the original.

	if (pCommand->command_number == 0)
		return bReturn;

	try
	{
		CBaseEntity* pBaseEntity = GetBaseEntity(me); //Grab the local player's entity pointer.

		if (pBaseEntity == NULL) //This should never happen, but never say never. 0xC0000005 is no laughing matter.
			return bReturn;

		int iFlags = gOffsets.GetFlags(pBaseEntity);
		static bool bJumpReleased = true;
		if (pCommand->buttons & IN_JUMP)
		{
			if (bJumpReleased == false)
			{
				if (gOffsets.GetWaterLevel(pBaseEntity) == 0 && !(iFlags & FL_ONGROUND)) //Double jump.
					pCommand->buttons &= ~IN_JUMP;
			}
			else
			{
				bJumpReleased = false;
			}
		}
		else if (bJumpReleased == false)
		{
			bJumpReleased = true;
		}

		bool bAimbotKeyPressed = GetAsyncKeyState(VK_XBUTTON2) & 0x8000;
		if (bAimbotKeyPressed)
		{
			gAimbot.FindTarget();
			if (gAimbot.bHasTarget())
			{
				gInts.Engine->SetViewAngles(pCommand->viewangles); //We're going to return false from CreateMove, so the game isn't going to set the view angles, so we need to do it manually.
				gAimbot.SilentAimFix(pCommand, gAimbot.GetAimbotAngles()); //Then set the silent aim angles.
				bReturn = false;
			}
		}

		if (GetAsyncKeyState(VK_XBUTTON1) & 0x8000)
		{
			if (gAimbot.bTriggerbotTrace(pCommand) || (gAimbot.bHasTarget() && bAimbotKeyPressed))
			{
				pCommand->buttons |= IN_ATTACK;
			}
		}

		if (pCommand->buttons & IN_ATTACK)
		{
			if ( bReturn == true)
				gInts.Engine->SetViewAngles(pCommand->viewangles); //We're going to return false from CreateMove, so the game isn't going to set the view angles, so we need to do it manually.
			Vector vecPunAngles = gOffsets.GetPunchAngles(pBaseEntity);
			pCommand->viewangles.x -= vecPunAngles.x;
			pCommand->viewangles.y -= vecPunAngles.z;
			bReturn = false;
		}
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Hooked_CreateMove");
	}
	return bReturn;
}
//============================================================================================
const char* CBaseEntity::GetPlayerName()
{
	//This could be done better...
	typedef IGameResources* (__stdcall* GetGameResources_t)(void);
	static GetGameResources_t GetGameResourcesCall = (GetGameResources_t)gBaseAPI.GetClientSignature("A1 ? ? ? ? 85 C0 74 06 05");

	return GetGameResourcesCall()->GetPlayerName(this->GetIndex());
}