#include "SDK.h"
#include "Panels.h"
#include "Client.h"

CScreenSize gScreenSize;
//===================================================================================
void __fastcall Hooked_PaintTraverse( PVOID pPanels, int edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce )
{
	try
	{
		VMTManager& hook = VMTManager::GetHook(pPanels); //Get a pointer to the instance of your VMTManager with the function GetHook.
		hook.GetMethod<void(__thiscall*)(PVOID, unsigned int, bool, bool)>(gOffsets.iPaintTraverseOffset)(pPanels, vguiPanel, forceRepaint, allowForce); //Call the original.

		static unsigned int vguiMatSystemTopPanel;

		if (vguiMatSystemTopPanel == NULL)
		{
			const char* szName = gInts.Panels->GetName(vguiPanel);
			if( szName[0] == 'M' && szName[3] == 'S' ) //Look for MatSystemTopPanel without using slow operations like strcmp or strstr.
			{
				vguiMatSystemTopPanel = vguiPanel;
				Intro();
			}
		}
		
		if ( vguiMatSystemTopPanel == vguiPanel ) //If we're on MatSystemTopPanel, call our drawing code.
		{
			if (gInts.Engine->IsDrawingLoadingImage() || !gInts.Engine->IsInGame() || !gInts.Engine->IsConnected() || gInts.Engine->Con_IsVisible() || ((GetAsyncKeyState(VK_F12) || gInts.Engine->IsTakingScreenshot())))
			{
				return; //We don't want to draw at the menu.
			}

			if (GetAsyncKeyState(VK_F10) & 0x8000)
			{
				try
				{
					Vector vScreen, vWorldPos;
					for (int iIndex = 0; iIndex <= gInts.EntList->GetHighestEntityIndex(); iIndex++)
					{
						CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

						if (pBaseEntity == NULL)
							continue;

						pBaseEntity->GetWorldSpaceCenter(vWorldPos);
						if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
							continue;

						if (pBaseEntity->GetClientClass()->iClassID == 0)
						{
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()));
						}
						else
						{
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, "%s %i", pBaseEntity->GetClientClass()->chName, pBaseEntity->GetClientClass()->iClassID);
						}
						static bool bLogEnts;
						if (!bLogEnts)
						{
							gBaseAPI.LogToFile("Begin logging Classes");
							for (ClientClass* pCC = gInts.Client->GetAllClasses(); pCC; pCC = pCC->pNextClass)
							{
								gBaseAPI.LogToFile("Class: %s Recv: %s ID: %i", pCC->chName, pCC->Table->GetName(), pCC->iClassID);
							}
							gBaseAPI.LogToFile("End of logging Classes");
							bLogEnts = true;
						}
					}
				}
				catch (...)
				{
					gBaseAPI.LogToFile("Failed DrawESP");
				}
			}
			else
			{
				gDrawManager.DrawString((gScreenSize.iScreenWidth / 2), 200, gDrawManager.dwGetTeamColor(3), "Welcome to Darkstorm"); //Remove this if you want.
				Vector vScreen, vWorldPos;

				CBaseEntity* pBaseLocalEnt = GetBaseEntity(me);

				if (pBaseLocalEnt == NULL)
					return;

				for (int iIndex = 0; iIndex <= gInts.EntList->GetHighestEntityIndex(); iIndex++)
				{
					CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

					if (pBaseEntity == NULL)
						continue;

					const char* chName = pBaseEntity->GetClientClass()->chName;

					if (!strcmp(chName, "CAI_BaseNPC"))
					{
						if (gOffsets.IsAlive(pBaseEntity))
						{
							const char* chModelName = gInts.ModelInfo->GetModelName(pBaseEntity->GetModel());
							if (strstr(chModelName, "scientist"))
							{
								if (strstr(chModelName, "female"))
									vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 16);
								else
									vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);

								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;

								if (strstr(chModelName, "eli"))
									gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, "Eli");
								else if (strstr(chModelName, "kliener"))
									gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, "Kliener");
								else
									gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, "Scientist");
								continue;
							}

							if (strstr(chModelName, "zombie"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Zombie");
								continue;
							}

							if (strstr(chModelName, "guard"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0x0026FFFF, "Guard");
								continue;
							}

							if (strstr(chModelName, "marine"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0x007F0EFF, "Marine");
								continue;
							}

							if (strstr(chModelName, "sentry"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 1);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF0000FF, "Sentry");
								continue;
							}

							if (strstr(chModelName, "bullsquid"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Bullsquid");
								continue;
							}

							if (strstr(chModelName, "controller"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Controller");
								continue;
							}

							if (strstr(chModelName, "garg"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Garg");
								continue;
							}

							if (strstr(chModelName, "houndeye"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 1);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Houndeye");
								continue;
							}

							if (strstr(chModelName, "agrunt"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 3);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0x8B4513FF, "AGrunt");
								continue;
							}

							if (strstr(chModelName, "headcrab") && !strstr(chModelName, "ss_"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Headcrab");
								continue;
							}

							if (strstr(chModelName, "ichthyosaur"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 1);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Ichthyosaur");
								continue;
							}

							if (strstr(chModelName, "xtroller"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 12);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Nihilanth");
								continue;
							}

							if (strstr(chModelName, "snark"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Snark");
								continue;
							}

							if (strstr(chModelName, "apache"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0x007F0EFF, "Apache");
								continue;
							}

							if (strstr(chModelName, "abrams") || strstr(chModelName, "lav.mdl"))
							{
								vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
								if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
									continue;
								gDrawManager.DrawString(vScreen.x, vScreen.y, 0x007F0EFF, "Tank");
								continue;
							}
						}
						continue;
					}

					if (!strcmp(chName, "CNPC_Barnacle"))
					{
						if (gOffsets.IsAlive(pBaseEntity))
						{
							Vector vecBase = gOffsets.GetBarnacleBase(pBaseEntity);
							Vector vecBaseScreen;
							bool bWasBaseVis = gDrawManager.WorldToScreen(vecBase, vecBaseScreen);
							bool bDrawLine = true;
							if (bWasBaseVis)
								gDrawManager.DrawString(vecBaseScreen.x, vecBaseScreen.y, 0x7F0000FF, "Barnacle");
							else
								bDrawLine = gMiscFuncs.FixTenticlePositionForOffscreen(vecBase, vecBaseScreen);
							Vector vecTenticle = gOffsets.GetBarnacleTenticle(pBaseEntity);
							Vector vecTenticleScreen;
							if (!gDrawManager.WorldToScreen(vecTenticle, vecTenticleScreen) && bDrawLine == true)
								bDrawLine = gMiscFuncs.FixTenticlePositionForOffscreen(vecTenticle, vecTenticleScreen);
							if (bDrawLine)
								gDrawManager.DrawLine(vecBaseScreen, vecTenticleScreen, 0x6F6F3FFF);
						}
						continue;
					}

					if (!strcmp(chName, "CNPC_XortEB"))
					{
						if (gOffsets.IsAlive(pBaseEntity))	
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0x007F0EFF, "Vortigaunt");
						}
						continue;
					}

					if (!strcmp(chName, "CNPC_Gonarch"))
					{
						if (gOffsets.IsAlive(pBaseEntity))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Gonarch");
							continue;
						}
					}

					if (!strcmp(chName, "CNPC_Zombie_Hev"))
					{
						if (gOffsets.IsAlive(pBaseEntity))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFE630FF, "Zombie");
						}
						continue;
					}

					if (!strcmp(chName, "CNPC_Human_Assassin"))
					{
						if (gOffsets.IsAlive(pBaseEntity))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 2);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0x404040FF, "Assassin");
							continue;
						}
					}

					if (!strcmp(chName, "CBaseAnimating"))
					{
						const char* chModelName = gInts.ModelInfo->GetModelName(pBaseEntity->GetModel());
						string modelname = chModelName;
						int iPosition = modelname.find("w_");
						if (iPosition > 10)
						{
							string chWeaponName = modelname.substr(iPosition + 2);
							chWeaponName.resize(chWeaponName.size() - 4);
							chWeaponName[0] = toupper((unsigned char)chWeaponName[0]);
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF6A00FF, chWeaponName.c_str());
						}
						continue;
					}

					if (!strcmp(chName, "CGrenade_Hornet"))
					{
						vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
						if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
							continue;
						gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF0000FF, "Hornet");
						continue;
					}

					if (!strcmp(chName, "CItem_Battery"))
					{
						vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
						if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
							continue;
						gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF6A00FF, "Battery");
						continue;
					}

					if (!strcmp(chName, "CRagdollProp"))
					{
						const char* chModelName = gInts.ModelInfo->GetModelName(pBaseEntity->GetModel());
						if (strstr(chModelName, "medkit"))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF6A00FF, "Healthkit");
						}

						continue;
					}

					if (!strcmp(chName, "CNPC_Sentry_Ground"))
					{
						if (gOffsets.IsAlive(pBaseEntity))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 1);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF0000FF, "Sentry");
						}
						continue;
					}

					if (!strcmp(chName, "CBlackMesaBaseDetonator"))
					{
						const char* chModelName = gInts.ModelInfo->GetModelName(pBaseEntity->GetModel());
						if (strstr(chModelName, "trip"))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF0000FF, "Tripmine");
						}
						continue;
					}

					if (!strcmp(chName, "CBlackMesaBaseGrenade"))
					{
						const char* chModelName = gInts.ModelInfo->GetModelName(pBaseEntity->GetModel());
						if (strstr(chModelName, "grenade"))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF0000FF, "Grenade");
						}
						continue;
					}

					if (!strcmp(chName, "CBlackMesaBaseProjectile"))
					{
						const char* chModelName = gInts.ModelInfo->GetModelName(pBaseEntity->GetModel());
						if (strstr(chModelName, "rpg"))
						{
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF0000FF, "RPG");
						}
						continue;
					}

					if (!strcmp(chName, "CBasePickup"))
					{
						const char* chModelName = gInts.ModelInfo->GetModelName(pBaseEntity->GetModel());
						string modelname = chModelName;
						int iPosition = modelname.find("w_");
						if (iPosition < 10)
						{
							if (strstr(chModelName, "xenians"))
								iPosition = 13;
						}
						if (iPosition > 10)
						{
							string chWeaponName = modelname.substr(iPosition + 2);
							chWeaponName.resize(chWeaponName.size() - 4);
							chWeaponName[0] = toupper((unsigned char)chWeaponName[0]);
							vWorldPos = gAimbot.vecGetHitbox(pBaseEntity, 0);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF6A00FF, chWeaponName.c_str());
							continue;
						}
					}

					if (strstr(chName, "CWeapon_"))
					{
						if (gOffsets.GetWeaponOwner(pBaseEntity) == NULL)
						{
							pBaseEntity->GetWorldSpaceCenter(vWorldPos);
							if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
								continue;
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFF6A00FF, chName + 8);
						}
					}
				}
			}
		}
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed PaintTraverse");
		gBaseAPI.ErrorBox("Failed PaintTraverse");
	}
}
//===================================================================================
void Intro( void )
{
	try
	{
		gDrawManager.Initialize(); //Initalize the drawing class.
		gOffsets.Initialize(); //Get the NetVar offsets.
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Intro");
		gBaseAPI.ErrorBox("Failed Intro");
	}
}