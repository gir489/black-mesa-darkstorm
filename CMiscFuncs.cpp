#include "CMiscFuncs.h"

CMiscFuncs gMiscFuncs;

bool CMiscFuncs::FixTenticlePositionForOffscreen(Vector& vecToFix, Vector& vecToFixScren)
{
	int iCounter = 100;
	float zBackup = vecToFix.z;
	while (iCounter > 0)
	{
		if (iCounter > 50)
			vecToFix.z -= 10;
		else
		{
			if (iCounter == 50)
				vecToFix.z = zBackup;
			vecToFix.z += 10;
		}
		if (gDrawManager.WorldToScreen(vecToFix, vecToFixScren))
			return true;
		iCounter--;
	}
	return false;
}