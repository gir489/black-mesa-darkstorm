#pragma once

#include "SDK.h"

class CMiscFuncs
{
public:
	bool FixTenticlePositionForOffscreen(Vector& vecToFix, Vector& vecToFixScren);
}; 

extern CMiscFuncs gMiscFuncs;